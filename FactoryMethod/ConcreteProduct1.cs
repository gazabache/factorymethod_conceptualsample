﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class ConcreteProduct1 : IProduct
    {
        public string Operacion()
        {
            return "Resultado de Concrete product 1";
        }
    }
}
