﻿using System;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("factory method!");

            Console.WriteLine("CONCRETE 1");
            ClientCode(new ConcreteCreator1());

            Console.WriteLine("CONCRETE 2");
            ClientCode(new ConcreteCreator2());

            Console.WriteLine();
        }

        public static void ClientCode(Creator creator)
        {
            Console.WriteLine("Hey no se con que clase derivada de creador trabajaré, aún así funciono" + creator.SomeOperation());

        }
    }
}
